<?php
/**
 * Created by PhpStorm.
 * User: yuriy
 * Date: 05.04.16
 * Time: 13:12
 */

namespace Core\Models;


use Core\Services\DB;
use Core\Services\Slayer;

/**
 * Class Core
 * @var Slayer $slayer
 * @package Core\Models
 * @var $db DB
 */
class Core
{
    protected $attributes = [];
    protected $primary_key = 'id';
    protected $table;
    protected $layer = null;
    protected $take = 500;
    protected $skip = 0;

    private $db;
    private $slayer;
    private $data = '*';

    protected $relations = [];

    public function __construct()
    {
        $this->slayer = new Slayer($this->layer);
        $this->db = DB::_connect();
        $this->data = ($this->attributes && !empty($this->attributes)) ? implode(',', $this->attributes) : '*';
    }

    /**
     * @return mixed
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->primary_key;
    }

    /**
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    public function getSlayer()
    {
        return $this->slayer;
    }

    public function getOne($value, $field = null)
    {
        $field = ($field) ? $field : $this->getPrimaryKey();
        $result = mysqli_query(DB::_connect(),
            'SELECT ' . $this->data . ' ' .
            'FROM ' . $this->table . ' ' .
            'WHERE ' . $field . '="' . $value . '"');
        $model = mysqli_fetch_assoc($result);
        if ($model && !empty($model)) {
            $relation = $this->_relation($value, $field);
            return array_merge($model, ['relation' => $relation]);
        }
        return [];
    }

    /**
     * @param $value
     * @param $field
     * @return array
     */
    private function _relation($value, $field)
    {
        /**
         * @var $relation Core
         */
        foreach ($this->relations as $foreign_key => $relation) {
            $relation = new $relation();
            $result[$relation->getTable()] = mysqli_fetch_assoc(mysqli_query(DB::_connect(),
                ' SELECT ' . $relation->getTable() . '.' . $relation->getData() .
                ' FROM ' . $relation->getTable() . ',' . $this->getTable() .
                ' WHERE ' . $relation->getTable() . '.' . $this->getPrimaryKey() . '=' . $this->getTable() . '.' . $foreign_key .
                ' AND ' . $this->getTable() . '.' . $field . '="' . $value . '"'
            ));
        }
        if (isset($result) && !empty($result)) {
            return $result;
        }
        return [];
    }

    public function getAllTest($limit = 20, $page = 1)
    {
        $result = mysqli_query(DB::_connect(),
            ' SELECT ' . $this->data .
            ' FROM ' . $this->table .
            ' ORDER BY ' . $this->getPrimaryKey() .  ' DESC ' .
            ' LIMIT ' . $limit .
            ' OFFSET ' . ($page - 1) * $limit);
        return ($result) ? mysqli_fetch_all($result, MYSQLI_ASSOC) : [];
    }

    public function insert($data)
    {
        $values = '"' . implode('","', array_values($data)) . '"';
        $result = mysqli_query(DB::_connect(), $sql = "
            INSERT INTO " . $this->table . "
            (" . implode(",", array_keys($data)) . ")
            VALUES (" . $values . ")
        ");
        return $result;
    }
}