<?php
/**
 * Created by PhpStorm.
 * User: yuriy
 * Date: 05.04.16
 * Time: 13:08
 */

namespace Core\Controllers;

/**
 * Class Core
 * @property \Core\Models\Core $model
 * @package Core\Controllers
 */
class Core
{
    protected $model = \Core\Models\Core::class;
    
    public function __construct()
    {
        $this->model = new $this->model();
    }

    public function w404(){
        $this->model->getSlayer()->display('404');
    }

    public function index(){
        $this->model->getSlayer()->display('default');
    }

    public function info(){
        phpinfo();
    }
    
}