<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 09.04.2016
 * Time: 19:01
 */

namespace Core\Services;

class DB
{
    private $host = 'localhost';
    private $user = 'root';
    private $password = 'password';
    private $base = 'test';
    private $connector;


    public function __construct()
    {
        $this->host = ($_ENV['DB_HOST']) ? $_ENV['DB_HOST'] : $this->host;
        $this->user = ($_ENV['DB_USER']) ? $_ENV['DB_USER'] : $this->user;
        $this->password = ($_ENV['DB_PASSWORD']) ? $_ENV['DB_PASSWORD'] : $this->password;
        $this->base = ($_ENV['DB_BASE']) ? $_ENV['DB_BASE'] : $this->base;
        $this->connector = $this->connect();
    }

    /**
     * @return mixed
     */
    public function connect()
    {
        @$connector = mysqli_connect($this->host, $this->user, $this->password, $this->base);
        return $connector;
    }

    /**
     * @return mixed
     */
    public function getConnector()
    {
        return $this->connector;
    }

    public static function _connect(){
        $HOST = ($_ENV['DB_HOST'])      ? $_ENV['DB_HOST']      : 'localhost';
        $USER = ($_ENV['DB_USER'])      ? $_ENV['DB_USER']      : 'root';
        $PASS = ($_ENV['DB_PASSWORD'])  ? $_ENV['DB_PASSWORD']  : 'password';
        $BASE = ($_ENV['DB_BASE'])      ? $_ENV['DB_BASE']      : 'base';
        @$CONNECTOR = mysqli_connect($HOST,$USER,$PASS,$BASE);
        return $CONNECTOR;
    }
}