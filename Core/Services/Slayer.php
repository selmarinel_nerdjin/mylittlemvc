<?php
/**
 * Created by PhpStorm.
 * User: yuriy
 * Date: 05.04.16
 * Time: 15:25
 */

namespace Core\Services;

use Core\AutoLoader;

final class Slayer
{
    private $slayer_dir;
    private $data = [];

    public function __construct($slayer_dir = null)
    {
        if (!$slayer_dir) $slayer_dir = AutoLoader::_ROOT() . 'Core/Views/';
        $this->slayer_dir = $slayer_dir;
    }

    public function set($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function delete($name)
    {
        unset($this->data[$name]);
    }

    public function __get($name)
    {
        if (isset($this->data[$name])) {
            return $this->data[$name];
        }
        return true;
    }

    /**
     * @param $template
     * @param null $data
     */
    public function display($template, $data = null)
    {
        $slayer = $this->slayer_dir . $template . ".slayer";
        if (!$slayer || !is_file($slayer)) {
            $slayer = AutoLoader::_ROOT() . 'Core/Views/404.slayer';
        }
        $ch = new Cache();
        $ch->readCache($template);
        if (is_array($data) && !empty($data)) {
            extract($data);
        }
        ob_start();
        include_once realpath($slayer);
        echo ob_get_clean();

        $ch->writeCache();
    }
}
