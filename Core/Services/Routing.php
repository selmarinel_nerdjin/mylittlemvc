<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 05.04.2016
 * Time: 23:56
 */

namespace Core\Services;


use Core\Controllers\Core;

class Routing
{

    protected $route = [];

    public function Route($uri, $controller)
    {
        $this->route[$uri] = $controller;
    }

    public function start()
    {
        $uri = explode('?', $_SERVER['REQUEST_URI']);
        $uri = str_replace('/', '', $uri[0]);
        $routes = $this->route;
        /**
         * @var Core $data
         */
        $data = new Core();
        $return = [];
        foreach ($routes as $rUri => $rRoute) {
            $rUri = substr($rUri, 1);
            if (preg_match("#^{$rUri}$#Ui", $uri)) {
                $route = preg_replace("#^{$rUri}$#Ui", $rRoute, $uri);
                $route = explode('/', $route);
                $return['controller'] = array_shift($route);
                $return['action'] = array_shift($route);
                $return['params'] = empty($route) ? array() : $route;
                if ($return['controller']) {
                    /**
                     * @var Core $controller
                     */
                    $controller = new $return['controller']();
                    if ($return['action']) {
                        $action = $return['action'];
                        $controller->$action($return['params']);
                        return true;
                    }
                    $data->w404();
                    return false;
                }
                ($uri === '/') ? $data->index() : $data->w404();
                return false;
            }
        }
        ($uri === '/') ? $data->index() : $data->w404();
        return false;
    }
}