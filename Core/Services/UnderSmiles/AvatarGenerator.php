<?php

namespace Core\Services\UnderSmiles;

class AvatarGenerator
{
    static function generateImage($str)
    {
        $secret = "SECRET_STRING";
        if (!isset($str)) {
            $str = sha1(rand(-99999, 99999) . $secret);
        } else {
            $str = sha1($str . $secret);
        }
        $WH = 3;
        $im = imagecreatetruecolor($WH, $WH);
        $im1 = imagecreatetruecolor($WH, $WH);
        for ($i = 0; $i < 6 * $WH + 1; $i++) {
            $str .= sha1($str . $secret);
        }
        $arr = array();
        for ($i = 0; $i < round(strlen($str) / 6, 0); $i++) {
            $arr[] = self::hex2rgb(substr($str, $i * 6, 6));
        }
        for ($i = 0; $i < $WH / 2; $i++) {
            $r = $arr[$i][0];
            $g = $arr[$i][1];
            $b = $arr[$i][2];
            $color = imagecolorallocate($im, $r, $g, $b);
            imagefilledrectangle($im1, $i, $i, $WH - $i, $WH - $i, $color);
        }
        imagecopy($im, $im1, 0, 0, 0, 0, $WH, $WH);;
        return $im;
    }

    static function hex2rgb($hex)
    {
        $hex = str_replace("#", "", $hex);
        if (strlen($hex) == 3) {
            $r = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
            $g = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
            $b = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
        } else {
            $r = hexdec(substr($hex, 0, 2));
            $g = hexdec(substr($hex, 2, 2));
            $b = hexdec(substr($hex, 4, 2));
        }
        $rgb = array($r, $g, $b);
        return $rgb;
    }
}

header("Content-type: image/png");
imagepng( AvatarGenerator::generateImage($_GET['name']));

