<?php
/**
 * Created by PhpStorm.
 * User: yuriy
 * Date: 14.04.16
 * Time: 16:09
 */

namespace Core\Services\UnderSmiles;


class Processor
{
    static protected $smilesPath = 'Core/Services/UnderSmiles/smiles/';
    static protected $devicesPath = 'Core/Services/UnderSmiles/devices/';

    static private $smiles = [
        //Sans smiles
        '%_%' => 'sans/sans.png',
        '*_*' => 'sans/sans-eyes.png',
        '*_-' => 'sans/sans-blink-right.png',
        ';)' => 'sans/sans-blink-right.png',
        '<_<' => 'sans/sans-look.png',
        '-_*' => 'sans/sans-blink-left.png',
        '-_-' => 'sans/-_-.png',
        'o_O' => 'sans/o_O.gif',
        'o_0' => 'sans/o_O.gif',
        'o_o' => 'sans/o_o.png',
        '0_0' => 'sans/o_o.png',
        '[:blink]' => 'sans/blink.gif',
        '[:slink]' => 'sans/slink.gif',
        '[:dance]' => 'sans/dance.gif',
        ';-)' => 'sans/blink.gif',
        //Papyrus smiles
        '%)' => 'papyrus/crazy.gif',
        '%-)' => 'papyrus/crazy.gif',
        '8)' => 'papyrus/8).gif',
        '8-)' => 'papyrus/8).gif',
        '[:smooth]' => 'papyrus/smooth.gif',
        '=)' => 'papyrus/=).gif',
        ':)' => 'papyrus/=).gif',
        ':-)' => 'papyrus/=).gif',
        '=D' => 'papyrus/=D.gif',
        ':D' => 'papyrus/=D.gif',
        ':-D' => 'papyrus/=D.gif',
        '[:evil_smile]' => 'papyrus/evil_smile.gif',
        '>_>' => 'papyrus/left_look.gif',
        '^_^' => 'papyrus/^_^.gif',
        '-_^' => 'papyrus/-_^.gif',
        ':hero' => 'papyrus/hero.gif',
        //Other
        'T_T' => 'other/T_T.gif',
        '=P' => 'other/=P.gif',
        '=p' => 'other/=P.gif',
        '[:lalala]' => 'other/lalala.gif',
        '[:1love]' => 'other/1love.gif',
        '[:boom]' => 'other/boom.gif',
        '}-{' => 'other/boom.gif',
        '[:bye]' => 'other/bye.gif',
        '[:cool]' => 'other/cool.gif',
        '[:cr]' => 'other/crazy.gif',
        '[:eval]' => 'other/eval.gif',
        '[:evil]' => 'other/evil.gif',
        '[:gast]' => 'other/gast.gif',
        '[:hat]' => 'other/hat.gif',
        '[:hihi]' => 'other/hihi.gif',
        '[:hoho]' => 'other/hoho.gif',
        '[:h0h0]' => 'other/hoho.gif',
        '[:hoh0]' => 'other/hoho.gif',
        '[:h0ho]' => 'other/hoho.gif',
        '[:horse]' => 'other/horse.gif',
        '[:hv]' => 'other/hovies.gif',
        '[:kl]' => 'other/kill_love.gif',
        '[:kiss]' => 'other/kiss.gif',
        '[:ld]' => 'other/left_dog.gif',
        '[:rd]' => 'other/right_dog.gif',
        '[:ma]' => 'other/maneken.gif',
        '[:meta]' => 'other/meta.gif',
        '[:mew]' => 'other/mew.gif',
        '[:oh]' => 'other/ohi.gif',
        '[:see]' => 'other/see.gif',
        '[:sh]' => 'other/shiki.gif',
        '[:think]' => 'other/think.gif',
        '[:wah]' => 'other/wah.gif',
        '[:whop]' => 'other/whop.gif',
        '[:woow]' => 'other/woow.gif',
        '[:wtf]' => 'other/wtf.gif',
    ];

    static private function updateUserSmiles($path = null)
    {

        if (!$path) {
            $path = self::$smilesPath . '/user/';
        }
        $files = scandir($path);
        foreach ($files as $file) {
            if ($file !== '.' && $file !== '..') {
                self::insertInSmailes($file);
            }
        }
    }

    static private function insertInSmailes($smile)
    {
        self::$smiles = array_merge(self::$smiles, ['[:' . substr($smile, 0, -4) . ']' => 'user/' . $smile]);
    }

    static private function getPath($smile)
    {
        return self::$smilesPath . $smile;
    }

    /**
     * @param string $code
     * @return string
     */
    static private function getSmile($code)
    {
        if (self::$smiles[$code] && file_exists(self::getPath(self::$smiles[$code]))) {
            return "<img src=" . self::getPath(self::$smiles[$code]) . " height='35' alt='{$code}' style='vertical-align:text-bottom;'>";
        }
        return $code;
    }

    static public function getScript()
    {
        return self::$smilesPath . '../' . 'UnderSmile.js';
    }

    static public function getStyle()
    {
        return self::$smilesPath . '../' . 'UnderSmile.css';
    }

    static public function analyze($text)
    {
        self::updateUserSmiles();
        $text = strip_tags($text);
        foreach (self::$smiles as $code => $smile) {
            $text = str_replace($code, self::getSmile($code), $text);
        }
        return nl2br($text);
    }

    static public function smilesList()
    {
        self::updateUserSmiles();
        $list = '<div class="smiles_list"> ';
        foreach (self::$smiles as $code => $smile) {
            $list = $list . "<span><img src=" . self::getPath(self::$smiles[$code]) . " height='30' alt='{$code}' style='vertical-align:text-bottom;'></span>";
        }
        return $list . '</div>';
    }

    static public function UnderSmile($rows = 40, $extended = '')
    {
        $smiley_block = '
       
        <div class="smiley_block">
            <textarea name="text" rows="' . $rows . '" placeholder="Your comment" required></textarea>';
        $smiley_block .= self::smilesList() . '
            <div class="smiles_panel">
                <span id="emoji">
                    <img src="' . self::$smilesPath . 'btn.png' . '">
                </span>
                ' . $extended . '
            </div>
        </div>';
        return $smiley_block;
    }

    const flatColors = [
        0 => '#2ecc71',
        1 => '#3498db',
        2 => '#9b59b6',
        3 => '#16a085',
        4 => '#f1c40f',
        5 => '#e67e22',
        6 => '#e74c3c',
        7 => '#d35400',
        8 => '#7f8c8d',
        9 => '#ffffff'
    ];

    public static function getDevice($device = ''){
        if ($device && file_exists(self::$devicesPath . $device . '.png')) {
            return "<img src=" . self::$devicesPath . $device . '.png' . " height='10'>";
        }
        return '';
    }
}