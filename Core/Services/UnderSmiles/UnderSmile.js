jQuery.fn.extend({
    insertAtCaret: function (myValue) {
        return this.each(function () {
            if (document.selection) {
                this.focus();
                var sel = document.selection.createRange();
                sel.text = myValue;
                this.focus();
            }
            else if (this.selectionStart || this.selectionStart == '0') {
                var startPos = this.selectionStart;
                var endPos = this.selectionEnd;
                var scrollTop = this.scrollTop;
                this.value = this.value.substring(0, startPos) + myValue + this.value.substring(endPos, this.value.length);
                this.focus();
                this.selectionStart = startPos + myValue.length;
                this.selectionEnd = startPos + myValue.length;
                this.scrollTop = scrollTop;
            } else {
                this.value += myValue;
                this.focus();
            }
        })
    }
});
$('.smiles_list img').on('click', function () {
    var $smile = $(this).attr('alt');
    $('.smiley_block textarea').insertAtCaret($smile);
});
$('#emoji').on('click', function () {
    $('.smiles_list').slideToggle('fast');
    view.hide();
});
var view = $('#smilesViewer');
$(document).ready(function () {
    $('.smiles_list img').on('mouseenter',function (pos) {
        view.css('left', (pos.pageX + 2) + 'px').css('top', (pos.pageY + 2) + 'px');
        view.find('img').attr('src', $(this).attr('src'));
        setTimeout(function () {
            view.show();
        }, 333);
    }).on('mouseout',function () {
        view.hide();
    });
});
view.on('mouseout',function () {
    view.hide();
})