<?php
/**
 * Created by PhpStorm.
 * User: yuriy
 * Date: 05.04.16
 * Time: 17:38
 */

namespace Core\Services;


use Core\AutoLoader;

class Cache
{
    private $CACHE_TIME = 0;
    private $cache_file;

    public function __construct()
    {
        if ($_ENV['CACHE_TIME']){
            $this->CACHE_TIME = (int) $_ENV['CACHE_TIME'];
        }
    }

    public function readCache($file = null)
    {
        if (!$file){
            $file = strrchr($_SERVER["SCRIPT_NAME"], "/");
            $file = substr($file, 1);
        }

        $this->cache_file = AutoLoader::_ROOT() . "Cache/" . md5($file) . ".html";
        if (file_exists($this->cache_file)) {
            if ((time() - $this->CACHE_TIME) < filemtime($this->cache_file)) {
                echo file_get_contents($this->cache_file);
                exit;
            }
        }
        ob_start();
    }

    public function writeCache()
    {
        if ($this->cache_file) {
            $handle = fopen($this->cache_file, 'w');
            fwrite($handle, ob_get_contents());
            fclose($handle);
            ob_end_flush();
        }
    }
}