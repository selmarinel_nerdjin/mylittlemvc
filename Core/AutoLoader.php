<?php
namespace Core {

    use Dotenv\Dotenv;

    class AutoLoader
    {
        const debug = 1;
        /**
         * @var \Dotenv\Dotenv
         */
        private $env;

        private static $root;

        public function __construct($root='/')
        {
            self::$root = $root;
            \spl_autoload_register('Core\AutoLoader::autoload');
            require_once('vendor/autoload.php');
            $this->env = new Dotenv(self::_ROOT(),'.env');
            $this->env->load();
        }

        public static function _ROOT()
        {
            return $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . self::$root . DIRECTORY_SEPARATOR;
        }

        public static function autoload($file)
        {
            $file = str_replace('\\', '/', $file);

            $path = self::_ROOT();
            $file_path = self::_ROOT() . $file . '.php';


            if (file_exists($file_path)) {

                if (AutoLoader::debug) AutoLoader::StPutFile(('Included ' . $file_path));
                require_once($file_path);

            } else {
                $flag = true;
                if (AutoLoader::debug) AutoLoader::StPutFile(('Begin recursive search file <b>' . $file . '</b> in <b>' . $path . '</b>'));
                return AutoLoader::recursive_autoload($file, $path, $flag);
            }
        }


        public static function recursive_autoload($file, $path, &$flag)
        {
            $res = false;
            if (false !== ($handle = opendir($path)) && $flag) {
                while (false !== ($dir = readdir($handle)) && $flag) {

                    if (strpos($dir, '.') === false) {
                        $path2 = $path . '/' . $dir;
                        $filepath = $path2 . '/' . $file . '.php';
                        if (AutoLoader::debug) AutoLoader::StPutFile(('Searching file <b>' . $file . '</b> in ' . $filepath));

                        if (file_exists($filepath)) {
                            $flag = false;
                            if (AutoLoader::debug) AutoLoader::StPutFile(('include ' . $filepath));
                            require_once($filepath);
                        }
                        $res = AutoLoader::recursive_autoload($file, $path2, $flag);
                    }
                }
                closedir($handle);
            }
            return $res;
        }

        private static function StPutFile($data)
        {
            //$dir = self::_ROOT() . 'Core/logs/.log';
            //@$file = fopen($dir, 'a+');
//            flock($file, LOCK_EX);
//            fwrite($file, ('|' . $data . '=>' . date('d.m.Y H:i:s') . '<br/>|<br/>' . PHP_EOL));
//            flock($file, LOCK_UN);
//            fclose($file);
        }
    }
}