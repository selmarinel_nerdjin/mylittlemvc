<?php
/**
 * Require AutoLoader 
 * It include self written Auto Loader and link to composer's auto loader
 * 
 * @var \Core\AutoLoader $app
 */
require_once('AutoLoader.php');
$app = new  \Core\AutoLoader($MY_ROOT);
/**
 * Start Route Service
 *
 * @var \Core\Services\Routing $route
 */
$route = new \Core\Services\Routing();



