<?php

function cleanCache($dir)
{
    $files = glob($dir . "/*");
    if (count($files) > 0) {
        foreach ($files as $file) {
            if (file_exists($file)) {
                unlink($file);
            }
        }
        return 'Success. Cache clean. ';
    }
    return 'No files. ';
}
function cleanLog(){
    if (file_exists('../logs/.log')) {
        $log = fopen('../logs/.log', 'w');
        fwrite($log, '');
        fclose($log);
        return 'Success. Log clean. ';
    }
    return 'Logs Fail. ';
}

foreach ($argv as $ar) {
    if ($ar == 'cl' || $ar == 'clear') {
        echo cleanCache('../../Cache');
        echo cleanLog();
        exit;
    }
}
echo "cl | clear to clear cache";
//cleanDir('../../Cache');