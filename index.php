<?php
/**
 * It's Important constant
 * It's your base root to your index.php file
 */
$MY_ROOT = '/';
/**
 * Include App core
 */
include_once('Core/app.php');
/**
 * Include Your Project routes
 */
require_once 'Ckaldaria/routes.php';
/**
 * Start Routing system
 * @var \Core\Services\Routing $route
 */
$route->start();