<?php
$route->Route('/info', '\Core\Controllers\Core/info');
$route->Route('/', '\Ckaldaria\Controllers\Main/index');
$route->Route('/switched', '\Ckaldaria\Controllers\Main/switched');

$route->Route('/send', '\Ckaldaria\Controllers\Comments/send');
$route->Route('/loadC', '\Ckaldaria\Controllers\Comments/loadComments');
$route->Route('/avatar', '\Ckaldaria\Controllers\Comments/avatar');

$route->Route('/shared', '\Ckaldaria\Controllers\Shared/index');




