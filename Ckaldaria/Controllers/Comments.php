<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 14.05.2016
 * Time: 9:17
 */

namespace Ckaldaria\Controllers;

use Core\Services\UnderSmiles\AvatarGenerator;
use Core\Services\UnderSmiles\Processor;
use Core\Controllers\Core;

/**
 * Class Comments
 * @property \Core\Models\Core $model
 * @package Ckaldaria\Controllers
 */
class Comments extends Core
{
    protected $model = \Ckaldaria\Models\Comments::class;

    public function loadComments()
    {
        $page = ($_GET['page']) ? $_GET['page'] : 1;
        $limit = ($_GET['take']) ? $_GET['take'] : 20;
        $comments = $this->model->getAllTest($limit, $page);
        return $this->generateCommentsList($comments);
    }

    public function send()
    {
        $data['name'] = $_GET['name'];
        $data['text'] = Processor::analyze($_GET['text']);
        $data['created_at'] = date('Y-m-d H:i:s', time());
        return $this->model->insert($data);
    }

    private function generateCommentsList(Array $comments)
    {
        $result = '';
        foreach ($comments as $comment) {
            $comment['device'] = Processor::getDevice($this->model->getDevice($comment['device_id']));
            $result .= self::oneViewComment($comment);
        }
        echo $result;
    }

    private static function oneViewComment($comment)
    {
        return
            " 
            <div class='comment_block'>
                <div class='comment_image'>
                    <img src='/avatar?name={$comment['name']}'>
                </div>
                <div class='comment_body'>
                    <div class='user_name'>{$comment['name']}</div>
                    <div class='comment_text'>{$comment['text']}</div>
                </div>
                <div class='comment_footer'>
                    <span class=''>{$comment['device']} {$comment['created_at']}</span>
                </div>
            </div>";
    }

    public function avatar()
    {
        return new AvatarGenerator();
    }
}