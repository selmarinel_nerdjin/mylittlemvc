<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 24.04.2016
 * Time: 11:35
 */

namespace Ckaldaria\Controllers;

use Ckaldaria\Models\Main;
use Core\AutoLoader;
use Core\Controllers\Core;

/**
 * Class Shared
 * @property \Core\Models\Core $model
 * @package Ckaldaria\Controllers
 */
class Shared extends Core
{
    protected $model = Main::class;

    public function index()
    {

        if (!empty($_FILES)) {
            $this->upload($_FILES);
        }
        $this->model->getSlayer()->display('shared', ['assets' => 'Ckaldaria/assets','list'=>$this->later()]);
    }

    private function upload(Array $data)
    {
        $uploaddir = AutoLoader::_ROOT() . 'Ckaldaria/uploads/';
        $uploadfile = $uploaddir . basename($data['uploadfile']['name']);
        if (copy($_FILES['uploadfile']['tmp_name'], $uploadfile)) {
            echo "<h3 class='alert alert-success'>Success</h3>";
        } else {
            echo "<h3 class='alert alert-danger'>Error</h3>";
            return;
        }
        echo "
        <span style='display: block;margin: 0 auto' class='btn btn-lg btn-success text-center'>Click here: 
        <b id='shared'>http://" . $_SERVER['HTTP_HOST'] . "/Ckaldaria/uploads/" . $data['uploadfile']['name'] . "</b>
        </span>";
    }

    private function later()
    {
        $dir = AutoLoader::_ROOT() . 'Ckaldaria/uploads/';
        $dh = opendir($dir);
        $files = [];
        while (false !== ($filename = readdir($dh))) {
            if ($filename != '.' && $filename != '..' && $filename != '.require') {
                $files[] = "http://" . $_SERVER['HTTP_HOST'] . "/Ckaldaria/uploads/" . $filename;
            }
        }
        return $files;
    }

}