<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 06.04.2016
 * Time: 20:55
 */

namespace Ckaldaria\Controllers;

use Core\Controllers\Core;

/**
 * Class Controller
 * @property \Core\Models\Core $model
 * @package Ckaldaria
 */
class Main extends Core
{
    protected $model = \Ckaldaria\Models\Main::class;
    
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->model->getSlayer()->display('index', [
            'assets' => 'Ckaldaria/assets',
            'team' => [
                [
                    'name' => 'Selmarinel',
                    'logo' => '/selmarinel.jpg',
                    'description' => 'Hell\'s Drummer'
                ],
                [
                    'name' => 'Freki',
                    'logo' => '/freki.jpg',
                    'description' => 'Bass MAN'
                ],
                [
                    'name' => 'Dizzy',
                    'logo' => '/dizi.jpg',
                    'description' => 'Saint Dizzy'
                ],
                [
                    'name' => 'Semo4ka',
                    'logo' => '/lenka.jpg',
                    'description' => 'Little Gitler'
                ],
                [
                    'name' => 'Shurik',
                    'logo' => '/alexsand.jpg',
                    'description' => '"Guitar HERO" &copy; champion'
                ],[
                    'name' => 'Til',
                    'logo' => '/til.jpg',
                    'description' => 'Plays on skinny flute'
                ],
//                [
//                    'name' => 'Kropa',
//                    'logo' => '/kropa.jpg',
//                    'description' => 'I don\'t know what is he doing here?'
//                ]
            ],
            //'comments'=>$this->comments->getData()
        ]);
    }

    public function switched()
    {
        if (!$_COOKIE['play']) {
            setcookie("play", true, time() + 3600);
        } else {
            setcookie("play", false);
        }
        $this->index();
    }

    
}