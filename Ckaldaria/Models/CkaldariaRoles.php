<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 09.04.2016
 * Time: 19:00
 */

namespace Ckaldaria\Models;


use Core\Models\Core;
use Core\Services\DB;

/**
 * Class Comments
 * @package Ckaldaria\Models
 */
class CkaldariaRoles extends Core
{
    protected $attributes = ['name', 'admin'];
    protected $table = 'ckaldaria_roles';
    protected $primary_key = 'id';

    public function __construct()
    {
        parent::__construct();
    }

}