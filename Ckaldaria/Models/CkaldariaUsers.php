<?php
/**
 * Created by PhpStorm.
 * User: Nerdjin
 * Date: 09.04.2016
 * Time: 19:00
 */

namespace Ckaldaria\Models;


use Core\Models\Core;

/**
 * Class Comments
 * @package Ckaldaria\Models
 */
class CkaldariaUsers extends Core
{
    protected $attributes = ['login', 'pass'];
    protected $table = 'ckaldaria_admin';
    protected $primary_key = 'id';
    protected $relations = [
        'role_id'   =>  CkaldariaRoles::class
    ];

    public function __construct()
    {
        parent::__construct();
    }

}