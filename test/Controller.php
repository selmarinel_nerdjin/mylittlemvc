<?php
/**
 * Created by PhpStorm.
 * User: yuriy
 * Date: 05.04.16
 * Time: 13:12
 */
namespace test;

use Core\Controllers\Core;

class Controller extends Core
{
    /**
     * @var Model $model
     */
    protected $model = Model::class;

    public function testSlayer()
    {
        $this->model->getSlayer()->display('index');
    }

}